const express = require('express');
const mongoose = require('mongoose');
const router = express.Router();

const UserSchema = mongoose.Schema({
    name: String,
    age: Number
});

const UserModel = mongoose.model('users', UserSchema);

/**
 * find all the items in the db and sends it as response
 */
router.get('/', (req, res) => {
    UserModel.find((err, res1) => {
        if(err) throw err;
        res.send(res1)
    })
});

/**
 * save the user to database
 */
router.post('/', (req, res) => {
    const userModel = new UserModel({name: req.body.name, age: req.body.age});
    userModel.save()
        .then(value => {
            console.log("record inserted successfully" + value.toString());
            res.sendStatus(200)
        }).catch(reason => console.log(reason.toString()))
});

module.exports = router;