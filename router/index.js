const express = require('express');
const router = express.Router();

router.use('/users', require('./users'));

router.post('/', (req, res) => {
    const name = req.body.name;
    const message = req.body.message;
    res.send({enteredName: name, enteredMessage: message})
});


module.exports = router;