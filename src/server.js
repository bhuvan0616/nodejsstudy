const express = require("express");
const mongoose = require("mongoose");
const bodyParser = require("body-parser");
const app = express();


app.use(bodyParser.urlencoded({extended: true}));
app.use('/', require('../router/index'));

mongoose.connect("mongodb://localhost:27017/Test_db")
    .then(value => console.log("mongoose connected successfully" + value.toString()))
    .catch(reason => console.log("db connection failed" + reason.toString()));

app.listen(3000, () => {
    console.log("listing to the port 3000...");
});